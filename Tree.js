export class Tree {
  constructor(x, y, w, h, hiddenItem) {
    this.drawX = x;
    this.drawY = y;
    this.width = w;
    this.height = h;
    this.leftX = this.drawX;
    this.rightX = this.drawX + this.width;
    this.topY = this.drawY;
    this.bottomY = this.drawY + this.height;
    this.srcY = 596;
    this.srcX = 544;
    this.hiddenItem = hiddenItem;
    this.visible = true;
  }

  update(imgSprite, ctxEntities) {
    ctxEntities.drawImage(imgSprite, this.srcX, this.srcY, this.width, this.height,
      this.drawX, this.drawY, this.width, this.height);
  };

  remove() {
    this.visible = false;
  }
}