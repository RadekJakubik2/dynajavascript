//BOMB
import { Storage } from "./Storage.js";

export class Bomb {
  constructor(ctxEntities) {
    this.srcXfive = 7;
    this.srcXfour = 58;
    this.srcXthree = 110;
    this.srcXtwo = 160;
    this.srcXone = 212;
    this.srcXexplosion = 0;
    this.srcX = this.srcXfive;
    this.srcY = 596;
    this.width = 42;
    this.height = 54;
    this.drawX = 0;
    this.drawY = 0;
    this.leftX = this.drawX;
    this.rightX = this.drawX + this.width;
    this.topY = this.drawY;
    this.bottomY = this.drawY + this.height;
    this.centerX = 0;
    this.centerY = 0;
    this.explodeIt = false;
    this.isPut = false;
    this.isLocked = false;
    this.ctxEntities = ctxEntities;
    this.explosionMethodArray = [];
    this.explosionMethodArray.push(this.explosionMethod);
  }

  explosionMethod = () => {
    this.srcX = this.srcXfive;
    this.explodeIt = true;
    console.log("explode it");
  }

  collideExplosion = () => {
    if (this.explosionMethodArray.length === 1 && this.isPut) {
      let removedMethod = this.explosionMethodArray.splice(0, 1);
      removedMethod[0]();
    }
  }

  explode(player) {
    console.log("explosion");
    for (let i = 0; i < 4; i++) {
      player.bullets.find(b => !b.isFlying).fire(this.centerX, this.centerY, i);
    }
    this.recycle();
  };

  recycle() {
    // likvidace objektu použité bomby  
    console.log("recycle");
    this.isPut = false;
    this.explodeIt = false;
    this.isLocked = false;
  };

  put(player) {
    this.isLocked = false;
    this.drawX = Math.floor(player.drawX / Storage.itemSide) * Storage.itemSide;
    this.drawY = Math.floor(player.drawY / Storage.itemSide) * Storage.itemSide;
    this.leftX = this.drawX;
    this.rightX = this.drawX + this.width;
    this.topY = this.drawY;
    this.bottomY = this.drawY + this.height;
    this.centerX = Storage.returnCenterX(this.drawX, this.width);
    this.centerY = Storage.returnCenterY(this.drawY, this.height);

    this.isPut = true;

    // v Inkscape udělat animaci, ušetřím tím spoustu výkonu!! 
    setTimeout(() => {
      if (this.explosionMethodArray.length === 1)
        this.explosionMethodArray[0]();
      else if (this.explosionMethodArray.length === 0)
        this.explosionMethodArray.push(this.explosionMethod);
    }, 3000);
  }

  update(imgSprite) {
    this.ctxEntities.drawImage(imgSprite, this.srcX, this.srcY, this.width, this.height, this.drawX, this.drawY, this.width, this.height);
  }
}