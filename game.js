//game
import { Storage } from "./Storage.js";
import { Stone } from "./Stone.js";
import { Player } from "./Player.js";
import { Enemy } from "./Enemy.js";
import { Tree } from "./Tree.js";
import { RollerSkates } from "./RollerSkates.js";

var canvasBg = document.getElementById("canvasBg"),
    ctxBg = canvasBg.getContext("2d"),
    canvasEntities = document.getElementById("canvasEntities"),
    ctxEntities = canvasEntities.getContext("2d"),
    bombs = [],
    enemies = [],
    stones = [],
    trees = [],
    itemsToPickUp = [],
    numEnemies = 50,
    isPlaying = false,
    requestAnimFrame = window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.oRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
        function (callback) {
            window.setTimeout(callback, 1000 / 60);
        },
    canvasWidth = Storage.canvasWidth,
    canvasHeight = Storage.canvasHeight;

var imgSprite = new Image();
imgSprite.src = "images/spriteDyna2.png";
imgSprite.addEventListener("load", init, false);
var player1 = new Player(bombs, enemies, stones, ctxEntities, imgSprite, trees, itemsToPickUp);

function init() {
    document.addEventListener("keydown", function (e) { checkKey(e, true); }, false);
    document.addEventListener("keyup", function (e) { checkKey(e, false); }, false);
    initStones();
    initTrees();
    initEnemies();
    startGame();
}

function startGame() {
    ctxBg.drawImage(imgSprite, 0, 0, canvasWidth, canvasHeight, 0, 0, canvasWidth, canvasHeight);
    //defineStones(); // pro testování, zobrazí v rozích černé čtverečky
    isPlaying = true;
    /* tells the browser that you wish to perform an animation and requests that the browser call 
       a specified function to update an animation before the next repaint.  */
    requestAnimFrame(gameLoop);
}

function gameLoop() {
    if (isPlaying) {
        update();
        requestAnimFrame(gameLoop);
    }
}

function clearCtx(ctx) {
    ctx.clearRect(0, 0, canvasWidth, canvasHeight);
}

function update() {
    clearCtx(ctxEntities);
    updateAllTrees();
    updateAllItemsToPickUp();
    updateAllEnemies();
    player1.update();
}

function updateAllEnemies() {
    for (var i = 0; i < enemies.length; i++) {
        // nepřítel se updatuje i po zabití (ležící mrtvola). Nutno po pár vteřinách odstranit ho úplně (z kolekce).        
        enemies[i].update(imgSprite);
    }
}

function updateAllItemsToPickUp() {
    for (var i = 0; i < itemsToPickUp.length; i++) {
        if (itemsToPickUp[i].visible) {
            itemsToPickUp[i].update(imgSprite, ctxEntities);
        } else {
            itemsToPickUp.splice(i, 1);
        }
    }
}

function updateAllTrees() {
    for (var i = 0; i < trees.length; i++) {
        if (trees[i].visible) {
            trees[i].update(imgSprite, ctxEntities);
        }
        else {
            trees[i].hiddenItem.create(trees[i])
            itemsToPickUp.push(trees[i].hiddenItem);
            trees.splice(i, 1);
        }
    }
}

function checkKey(e, value) {
    var keyID = e.keyCode || e.which;
    if (keyID === 38) { // Up arrow
        player1.isUpKey = value;
        e.preventDefault();
    }
    if (keyID === 39) { // Right arrow
        player1.isRightKey = value;
        e.preventDefault();
    }
    if (keyID === 40) { // Down arrow
        player1.isDownKey = value;
        e.preventDefault();
    }
    if (keyID === 37) { // Left arrow
        player1.isLeftKey = value;
        e.preventDefault();
    }
    if (keyID === 32) { // Spacebar
        player1.isSpacebar = value;
        e.preventDefault();
    }
}

function initEnemies() {
    for (var i = 0; i < numEnemies; i++) {
        //enemies[enemies.length] = new Enemy(bombs, stones, ctxEntities, trees);
    }
}

function initStones() {
    var boardXCount = 15;
    var boardYCount = 11;
    stones.length = 0;
    for (let x = 0; x < boardXCount; x++) {
        if (x % 2 === 0)
            continue;
        for (let y = 0; y < boardYCount; y++) {
            if (y % 2 === 0)
                continue;
            stones.push(new Stone(x * Storage.itemSide, y * Storage.itemSide,
                Storage.itemSide, Storage.itemSide, ctxBg));
        }
    }
}

function initTrees() {
    trees.push(new Tree(1 * Storage.itemSide, 2 * Storage.itemSide, Storage.itemSide, Storage.itemSide, new RollerSkates()));
    trees.push(new Tree(2 * Storage.itemSide, 3 * Storage.itemSide, Storage.itemSide, Storage.itemSide, new RollerSkates()));
    trees.push(new Tree(3 * Storage.itemSide, 2 * Storage.itemSide, Storage.itemSide, Storage.itemSide, new RollerSkates()));
    trees.push(new Tree(0 * Storage.itemSide, 4 * Storage.itemSide, Storage.itemSide, Storage.itemSide, new RollerSkates()));
    trees.push(new Tree(4 * Storage.itemSide, 0 * Storage.itemSide, Storage.itemSide, Storage.itemSide, new RollerSkates()));
}