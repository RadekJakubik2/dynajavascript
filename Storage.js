export class Storage {
  static itemSide = 54;
  static canvasWidth = canvasBg.width;
  static canvasHeight = canvasBg.height;
  static _false = -1;
  static debugBreakpoint = 1;

  static returnCenterX(drawX, width) {
    return drawX + (width / 2);
  }

  static returnCenterY(drawY, height) {
    return drawY + (height / 2);
  }

  static checkCollideConditions(leftX, rightX, topY, bottomY, charCenterX, charCenterY) {
    if (leftX < charCenterX && charCenterX < rightX && topY < charCenterY && charCenterY < bottomY)
      return true;
    return false;
  }

  static collision(a, b) {
    return a.drawX <= b.drawX + b.width &&
      a.drawX >= b.drawX &&
      a.drawY <= b.drawY + b.height &&
      a.drawY >= b.drawY;
  }

  static checkCollide(newDrawX, newDrawY, collection, _this) {
    let newCenterX = this.returnCenterX(newDrawX, _this.width),
      newCenterY = this.returnCenterY(newDrawY, _this.height);
    for (var i = 0; i < collection.length; i++) {
      var leftX = collection[i].leftX + _this.leftIndent;
      var rightX = collection[i].rightX + _this.rightIndent;
      var topY = collection[i].topY + _this.topIndent;
      var bottomY = collection[i].bottomY + _this.bottomIndent;

      if (this.checkCollideConditions(leftX, rightX, topY, bottomY, newCenterX, newCenterY)) {
        return collection[i];
      }
    }
    return this._false;
  };

  static exceededBombsPutLimit(bombs, maxOccurencies) {
    var count = 0;
    for (var i = 0; i < bombs.length; ++i) {
      if (bombs[i].isPut)
        count++;
      if (count >= maxOccurencies)
        return true;
    }
    return false;
  }

  static outOfBounds(a, x, y) {
    var newBottomY = y + a.height,
      newTopY = y,
      newRightX = x + a.width,
      newLeftX = x,
      treeLineTop = 0,
      treeLineBottom = 594,
      treeLineRight = 810,
      treeLineLeft = 0;
    return newBottomY > treeLineBottom ||
      newTopY < treeLineTop ||
      newRightX > treeLineRight ||
      newLeftX < treeLineLeft;
  }
}