import { ItemToPickUp } from "./ItemToPickUp.js";

export class RollerSkates extends ItemToPickUp {
  constructor() {
    super();
    this.srcY = 596;
    this.srcX = 212; // obrázek bomby 1 (prozatímně) 
  }

  update(imgSprite, ctxEntities) {
    ctxEntities.drawImage(imgSprite, this.srcX, this.srcY, this.width, this.height,
      this.drawX, this.drawY, this.width, this.height);
  };

  applyEffect(player) {
    player.speed *= 2;
  }

  remove() {
    this.visible = false;
  }
}