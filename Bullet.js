//BULLET
import { Storage } from "./Storage.js";

export class Bullet {
  constructor(enemies, stones, bombs, trees) {
    this.radius = 2;
    this.width = this.radius * 2;
    this.height = this.radius * 2;
    this.drawX = 0;
    this.drawY = 0;
    this.isFlying = false;
    this.xVel = 0;
    this.yVel = 0;
    this.speed = 6;
    this.enemies = enemies;
    this.stones = stones;
    this.bombs = bombs;
    this.trees = trees;
  }

  update(ctxEntities) {
    this.drawX += this.xVel;
    this.drawY += this.yVel;
    this.checkHitBomb();
    this.checkHitEnemy();
    this.checkHitTree();
    this.checkHitStone();
    this.checkOutOfBounds();

    ctxEntities.fillStyle = "white";
    ctxEntities.beginPath();
    ctxEntities.arc(this.drawX, this.drawY, this.radius, 0, Math.PI * 2, false);
    ctxEntities.closePath();
    ctxEntities.fill();
  };

  fire(startX, startY, shootNumber) {
    // var soundEffect = new Audio("audio/shooting.wav");
    // soundEffect.play();
    this.isFlying = true;
    this.drawX = startX;
    this.drawY = startY;

    if (shootNumber === 0) {
      this.xVel = 0;
      this.yVel = this.speed;
    } else if (shootNumber === 1) {
      this.xVel = 0;
      this.yVel = -this.speed;
    } else if (shootNumber === 2) {
      this.xVel = -this.speed;
      this.yVel = 0;
    } else if (shootNumber === 3) {
      this.xVel = this.speed;
      this.yVel = 0;
    }
  };

  recycle() {
    this.isFlying = false;
  };

  checkHitEnemy() {
    for (var i = 0; i < this.enemies.length; i++) {
      if (Storage.collision(this, this.enemies[i]) && !this.enemies[i].isDead) {
        this.recycle();
        this.enemies[i].die();
      }
    }
  };

  checkHitBomb() {
    let putBombs = this.bombs.filter(b => b.isPut);
    for (var i = 0; i < putBombs.length; i++) {
      if (Storage.collision(this, putBombs[i])) {
        this.recycle();
        //console.log("BOMB TOUCHED BOMB");
        putBombs[i].collideExplosion();
      }
    }
  }

  checkHitTree() {
    for (var i = 0; i < this.trees.length; i++) {
      if (Storage.collision(this, this.trees[i])) {
        this.recycle();
        this.trees[i].remove();
      }
    }
  };

  checkHitStone() {
    for (var i = 0; i < this.stones.length; i++) {
      if (Storage.collision(this, this.stones[i])) {
        this.recycle();
      }
    }
  };

  checkOutOfBounds() {
    if (Storage.outOfBounds(this, this.drawX, this.drawY)) {
      this.recycle();
    }
  };
}