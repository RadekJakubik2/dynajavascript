import { Figure } from "./Figure.js";
import { Bullet } from "./Bullet.js";
import { Bomb } from "./Bomb.js";
import { Storage } from "./Storage.js";

export class Player extends Figure {
  constructor(bombs, enemies, stones, ctxEntities, imgSprite, trees, itemsToPickUp) {
    super();
    this.srcX = 395;
    this.srcY = 596;
    this.width = 35;
    this.height = 54;
    this.drawX = 0;
    this.drawY = 0;
    this.centerX = Storage.returnCenterX(this.drawX, this.width);
    this.centerY = Storage.returnCenterY(this.drawY, this.height);
    this.speed = 2;
    this.isUpKey = false;
    this.isRightKey = false;
    this.isDownKey = false;
    this.isLeftKey = false;
    this.isSpacebar = false;
    this.numBullets = 100; //musel bych mít 26 bomb aby to nestačilo
    this.bullets = [];
    this.numBombs = 1; //max. povolený počet položených bomb hráčem v jeden moment
    this.allBombsStack = 40; //vygeneruje se na začátku hry, musí tam být dostatečná rezerva, aby vždy bylo hráči k dispozici numBombs připravených bomb!
    this.bombs = bombs;
    this.stones = stones;
    this.enemies = enemies;
    this.ctxEntities = ctxEntities;
    this.imgSprite = imgSprite;
    this.trees = trees;
    this.itemsToPickUp = itemsToPickUp;

    for (var i = 0; i < this.numBullets; i++) {
      this.bullets[this.bullets.length] = new Bullet(this.enemies, this.stones, this.bombs, this.trees);
    }
    /* NUTNO INICIALIZOVAT NA ZAČÁTKU HRY HODNĚ BOMB, a potom hráči povolit využívat jen určitou část. 
    Protože jedné bombě pár vteřin trvá než se stane znovu použitelnou v některých situacích, 
    proto je třeba mít rezervní, aby měl hráč po ruce vždy použitelné bomby.*/
    for (var i = 0; i < this.allBombsStack; i++) {
      this.bombs[this.bombs.length] = new Bomb(this.ctxEntities);
    }
  }

  update() {
    this.centerX = Storage.returnCenterX(this.drawX, this.width);
    this.centerY = Storage.returnCenterY(this.drawY, this.height);
    this.checkDirection();
    this.checkShooting();
    this.updateAllBombs();
    this.updateAllBullets();
    this.ctxEntities.drawImage(this.imgSprite, this.srcX, this.srcY, this.width,
      this.height, this.drawX, this.drawY, this.width, this.height);
  }

  checkDirection() {
    var newDrawX = this.drawX,
      newDrawY = this.drawY,
      stoneCollision = false;
    if (this.isUpKey) {
      newDrawY -= this.speed;
      this.srcX = 312; // Facing north
    } else if (this.isDownKey) {
      newDrawY += this.speed;
      this.srcX = 264; // Facing south
    } else if (this.isRightKey) {
      newDrawX += this.speed;
      this.srcX = 395; // Facing east
    } else if (this.isLeftKey) {
      newDrawX -= this.speed;
      this.srcX = 355; // Facing west
    }

    let itemCollisionObj = Storage.checkCollide(newDrawX, newDrawY, this.itemsToPickUp, this);
    if (itemCollisionObj != Storage._false) {
      itemCollisionObj.applyEffect(this);
      itemCollisionObj.remove();
    }
    let treeCollision = Storage.checkCollide(newDrawX, newDrawY, this.trees, this) != Storage._false;
    stoneCollision = Storage.checkCollide(newDrawX, newDrawY, this.stones, this) != Storage._false;
    let bombLockedCollide = this.bombLocking(newDrawX, newDrawY);

    if (!bombLockedCollide && !stoneCollision && !treeCollision &&
      !Storage.outOfBounds(this, newDrawX, newDrawY)) {
      this.drawX = newDrawX;
      this.drawY = newDrawY;
    }
  };

  bombLocking(newDrawX, newDrawY) {
    if (!this.bombs.filter(b => b.isPut))
      return false;
    var newCenterX = Storage.returnCenterX(newDrawX, this.width),
      newCenterY = Storage.returnCenterY(newDrawY, this.height);

    var putBombs = this.bombs.filter(b => b.isPut);
    for (let bomb of putBombs) {
      var leftX = bomb.drawX + this.leftIndent;
      var rightX = bomb.drawX + Storage.itemSide + this.rightIndent;
      var topY = bomb.drawY + this.topIndent;
      var bottomY = bomb.drawY + Storage.itemSide + this.bottomIndent;

      if (!bomb.isLocked &&
        !Storage.checkCollideConditions(leftX, rightX, topY, bottomY, newCenterX, newCenterY)) {
        bomb.isLocked = true;
      }
      else if (bomb.isLocked &&
        Storage.checkCollideConditions(leftX, rightX, topY, bottomY, newCenterX, newCenterY)) {
        return true;
      }
    }
    return false;
  }

  allowedToPutBombHere() {
    let drawX = Math.floor(this.drawX / Storage.itemSide) * Storage.itemSide;
    let drawY = Math.floor(this.drawY / Storage.itemSide) * Storage.itemSide;

    for (let i = 0; i < this.bombs.length; i++) {
      if (this.bombs[i].isPut && this.bombs[i].drawY == drawY && this.bombs[i].drawX == drawX) {
        return false;
      }
    }
    return true;
  }

  checkShooting() {
    if (this.isSpacebar && this.allowedToPutBombHere()) {
      Storage.debugBreakpoint++;
      if (!Storage.exceededBombsPutLimit(this.bombs, this.numBombs)) {
        var sleepingBomb = this.bombs.find(b => !b.isPut && b.explosionMethodArray.length === 1);
        if (sleepingBomb) {
          sleepingBomb.put(this);
        }
      }
    }
    this.isSpacebar = false;
  };

  updateAllBombs() {
    for (var i = 0; i < this.bombs.length; i++) {
      if (this.bombs[i].isPut) {
        this.bombs[i].update(this.imgSprite);
      }

      if (this.bombs[i].explodeIt) {
        this.bombs[i].explode(this);
      }
    }
  };

  updateAllBullets() {
    for (var i = 0; i < this.bullets.length; i++) {
      if (this.bullets[i].isFlying) {
        this.bullets[i].update(this.ctxEntities);
      }
    }
  };
}