//ENEMY
import { Figure } from "./Figure.js";
import { Storage } from "./Storage.js";

export class Enemy extends Figure {
    constructor(bombs, stones, ctxEntities, trees) {
        super();
        this.srcX = 437;
        this.srcY = 596;
        this.width = 45;
        this.height = 54;
        // this.drawX = randomRange(0, canvasWidth - this.width);
        // this.drawY = randomRange(0, canvasHeight - this.height);
        this.drawX = 0;
        this.drawY = 0;
        this.centerX = Storage.returnCenterX(this.drawX, this.width);
        this.centerY = Storage.returnCenterY(this.drawY, this.height);
        this.targetX = this.centerX;
        this.targetY = this.centerY;
        //this.randomMoveTime = randomRange(4000, 10000);
        this.speed = 1;
        var that = this;
        this.moveInterval = setInterval(function () { that.setTargetLocation(); }/* , that.randomMoveTime */);
        this.isDead = false;
        this.direction = 0;
        this.nextMovementValue = 0;
        this.resetDirection = true;
        this.directionsArray = [1, 2, 3, 4]; // nikdy odsud neodebírám
        this.wrongDirectionsArray = new Array(4);
        this.bombs = bombs;
        this.stones = stones;
        this.trees = trees;
        this.ctxEntities = ctxEntities;
    }

    update(imgSprite) {
        if (!this.isDead) {
            this.centerX = Storage.returnCenterX(this.drawX, this.width);
            this.centerY = Storage.returnCenterY(this.drawY, this.height);
            this.checkDirection();
        }
        this.ctxEntities.drawImage(imgSprite, this.srcX, this.srcY, this.width, this.height, this.drawX, this.drawY, this.width, this.height);
    };

    setTargetLocation() {
        //this.randomMoveTime = randomRange(4000, 10000);
        var minX = this.centerX - 50,
            maxX = this.centerX + 50,
            minY = this.centerY - 50,
            maxY = this.centerY + 50;
        if (minX < 0) {
            minX = 0;
        }
        if (maxX > Storage.canvasWidth) {
            maxX = Storage.canvasWidth;
        }
        if (minY < 0) {
            minY = 0;
        }
        if (maxY > Storage.canvasHeight) {
            maxY = Storage.canvasHeight;
        }
        this.targetX = this.randomRange(minX, maxX);
        this.targetY = this.randomRange(minY, maxY);
    };

    randomRange(min, max) {
        return Math.floor(Math.random() * (max + 1 - min)) + min;
    }

    getRandomInt(max) {
        return Math.floor(Math.random() * Math.floor(max)) + 1;
    }

    getRandomArrayElement(arr) {
        var min = 0;
        var max = arr.length;
        var randIndex = Math.floor(Math.random() * (max - min)) + min;
        return arr[randIndex];
    }

    evaluateDirection() {
        if (this.direction == 1) {
            // doleva (kontroluji cílové X)
            return this.drawX <= this.nextMovementValue;
        }
        else if (this.direction == 2) {
            // doprava (kontroluji cílové X)
            return this.drawX >= this.nextMovementValue;
        }
        else if (this.direction == 3) {
            // dolů (kontroluji cílové Y)
            return this.drawY >= this.nextMovementValue;
        }
        else if (this.direction == 4) {
            // nahoru (kontroluji cílové Y)
            return this.drawY <= this.nextMovementValue;
        }
        else {
            // počáteční nula po vzniku enemy například
            return true;
        }
    }

    computeNextMovementValue(direction) {
        var possibleMovements = 0;
        if (direction == 1) { //doleva
            // zjistit současnou drawX (doprava k ní ještě připočíst itemSide) a rozpočítat o kolik šířek může tím směrem
            possibleMovements = Math.floor(this.drawX / Storage.itemSide);
        }
        else if (direction == 2) {//doprava
            possibleMovements = Math.floor((Storage.canvasWidth - this.drawX + Storage.itemSide) / Storage.itemSide);
        }
        else if (direction == 3) {//dolů
            possibleMovements = Math.floor((Storage.canvasHeight - this.drawY + Storage.itemSide) / Storage.itemSide);
        }
        else if (direction == 4) {//nahoru
            possibleMovements = Math.floor(this.drawY / Storage.itemSide);
        }
        // vrátí to přesnou hodnotu mapy kam musí enemy dojít. Až tam dojde, nastaví se reset=true a tak pořád dokola
        return this.getRandomInt(possibleMovements) * Storage.itemSide;
    }

    checkDirection() {
        if (this.resetDirection) { // buď targetX nebo targetY podle směru
            this.direction = this.getRandomArrayElement(this.directionsArray.filter(num => !this.wrongDirectionsArray.includes(num)));
            this.nextMovementValue = this.computeNextMovementValue(this.direction);
        }

        var newDrawX = this.drawX,
            newDrawY = this.drawY,
            stoneCollision = false;

        if (this.direction == 1) {
            newDrawX -= this.speed; // doleva
        } else if (this.direction == 2) {
            newDrawX += this.speed; // doprava
        } else if (this.direction == 3) {
            newDrawY += this.speed; // dolů
        } else if (this.direction == 4) {
            newDrawY -= this.speed; // nahoru
        }

        let bombCollisionObj = Storage.checkCollide(newDrawX, newDrawY, this.bombs.filter(b => b.isPut && !b.explodeIt), this);
        if (bombCollisionObj !== Storage._false) {
            //console.log("ENEMY TOUCHED BOMB");
            bombCollisionObj.collideExplosion();
        }
        let treeCollision = Storage.checkCollide(newDrawX, newDrawY, this.trees, this) != Storage._false;
        stoneCollision = Storage.checkCollide(newDrawX, newDrawY, this.stones, this) != Storage._false;

        if (stoneCollision || treeCollision || Storage.outOfBounds(this, newDrawX, newDrawY)) {
            this.resetDirection = true;
            this.wrongDirectionsArray.push(this.direction);
            return;
        }
        else if (!stoneCollision && !treeCollision && !Storage.outOfBounds(this, newDrawX, newDrawY)) {
            this.drawX = newDrawX;
            this.drawY = newDrawY;
            this.wrongDirectionsArray = new Array(4);
        }

        this.resetDirection = this.evaluateDirection();
        // if (this.centerX < this.targetX) {
        //     this.drawX += this.speed;
        // } else if (this.centerX > this.targetX) {
        //     this.drawX -= this.speed;
        // }
        // if (this.centerY < this.targetY) {
        //     this.drawY += this.speed;
        // } else if (this.centerY > this.targetY) {
        //     this.drawY -= this.speed;
        // }
    };

    die() {
        // var soundEffect = new Audio("audio/dying.wav");
        // soundEffect.play();
        clearInterval(this.moveInterval);
        this.srcX = 492;
        this.isDead = true;
    };
}