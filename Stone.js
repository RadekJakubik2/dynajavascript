export class Stone {
  constructor(x, y, w, h, ctxBg) {
    this.drawX = x;
    this.drawY = y;
    this.width = w;
    this.height = h;
    this.leftX = this.drawX;
    this.rightX = this.drawX + this.width;
    this.topY = this.drawY;
    this.bottomY = this.drawY + this.height;

    // JEN PRO TESTOVACÍ ÚČELY! Pak vyjmout i arg z ctoru.
    ctxBg.fillText("AAA", this.leftX, this.topY, 5);
    ctxBg.fillText("AAA", this.leftX, this.bottomY, 5);
    ctxBg.fillText("AAA", this.rightX, this.topY, 5);
    ctxBg.fillText("AAA", this.rightX, this.bottomY, 5);
  }
}