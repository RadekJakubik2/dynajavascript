export class ItemToPickUp {

  create(tree) {
    this.drawX = tree.drawX;
    this.drawY = tree.drawY;
    this.width = tree.width;
    this.height = tree.height;
    this.leftX = this.drawX;
    this.rightX = this.drawX + this.width;
    this.topY = this.drawY;
    this.bottomY = this.drawY + this.height;
    this.visible = true;
  }


}